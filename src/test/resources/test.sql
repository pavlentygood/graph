insert into forest (id) values (1);
insert into forest (id) values (2);
insert into forest (id) values (3);
insert into forest (id) values (4);
insert into forest (id) values (5);

insert into tree (id, forest_id) values (1, 1);
insert into tree (id, forest_id) values (2, 1);
insert into tree (id, forest_id) values (3, 1);

insert into tree (id, forest_id) values (4, 2);
insert into tree (id, forest_id) values (5, 2);
insert into tree (id, forest_id) values (6, 2);

insert into tree (id, forest_id) values (7, 3);
insert into tree (id, forest_id) values (8, 3);
insert into tree (id, forest_id) values (9, 3);

insert into tree (id, forest_id) values (10, 4);
insert into tree (id, forest_id) values (11, 4);
insert into tree (id, forest_id) values (12, 4);

insert into tree (id, forest_id) values (13, 5);
insert into tree (id, forest_id) values (14, 5);
insert into tree (id, forest_id) values (15, 5);

insert into branch (id, tree_id) values (1, 1);
insert into branch (id, tree_id) values (2, 1);
insert into branch (id, tree_id) values (3, 1);

insert into branch (id, tree_id) values (4, 2);
insert into branch (id, tree_id) values (5, 2);
insert into branch (id, tree_id) values (6, 2);

insert into branch (id, tree_id) values (7, 3);
insert into branch (id, tree_id) values (8, 3);
insert into branch (id, tree_id) values (9, 3);

insert into branch (id, tree_id) values (10, 4);
insert into branch (id, tree_id) values (11, 4);
insert into branch (id, tree_id) values (12, 4);

insert into branch (id, tree_id) values (13, 5);
insert into branch (id, tree_id) values (14, 5);
insert into branch (id, tree_id) values (15, 5);

insert into branch (id, tree_id) values (16, 6);
insert into branch (id, tree_id) values (17, 6);
insert into branch (id, tree_id) values (18, 6);

insert into branch (id, tree_id) values (19, 7);
insert into branch (id, tree_id) values (20, 7);
insert into branch (id, tree_id) values (21, 7);

insert into branch (id, tree_id) values (22, 8);
insert into branch (id, tree_id) values (23, 8);
insert into branch (id, tree_id) values (24, 8);

insert into branch (id, tree_id) values (25, 9);
insert into branch (id, tree_id) values (26, 9);
insert into branch (id, tree_id) values (27, 9);

insert into branch (id, tree_id) values (28, 10);
insert into branch (id, tree_id) values (29, 10);
insert into branch (id, tree_id) values (30, 10);

insert into branch (id, tree_id) values (31, 11);
insert into branch (id, tree_id) values (32, 11);
insert into branch (id, tree_id) values (33, 11);

insert into branch (id, tree_id) values (34, 12);
insert into branch (id, tree_id) values (35, 12);
insert into branch (id, tree_id) values (36, 12);

insert into branch (id, tree_id) values (37, 13);
insert into branch (id, tree_id) values (38, 13);
insert into branch (id, tree_id) values (39, 13);

insert into branch (id, tree_id) values (40, 14);
insert into branch (id, tree_id) values (41, 14);
insert into branch (id, tree_id) values (42, 14);

insert into branch (id, tree_id) values (43, 15);
insert into branch (id, tree_id) values (44, 15);
insert into branch (id, tree_id) values (45, 15);

insert into leaf (id, branch_id) values (1, 1);
insert into leaf (id, branch_id) values (2, 20);
insert into leaf (id, branch_id) values (3, 45);
