package com.example.graph;

import com.example.graph.repo.ForestRepo;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@Sql("classpath:test.sql")
@SpringBootTest
class GraphApplicationTests {

    @Autowired
    ForestRepo forestRepo;

    @Transactional
    @Test
    void test1() {
        var leavesCount = forestRepo.findAll().stream()
                .map(f -> f.getTrees().stream()
                        .map(t -> t.getBranches().stream()
                                .map(b -> b.getLeaves().size())
                                .reduce(Integer::sum)
                                .orElseThrow())
                        .reduce(Integer::sum)
                        .orElseThrow())
                .reduce(Integer::sum)
                .orElseThrow();

        assert leavesCount == 3;
    }

    @Test
    void test2() {
        forestRepo.findById(3);
    }

    @Transactional
    @Test
    void test3() {
        var leavesCount = forestRepo.findAllById(1).stream()
                .map(f -> f.getTrees().stream()
                        .map(t -> t.getBranches().stream()
                                .map(b -> b.getLeaves().size())
                                .reduce(Integer::sum)
                                .orElseThrow())
                        .reduce(Integer::sum)
                        .orElseThrow())
                .reduce(Integer::sum)
                .orElseThrow();

        assert leavesCount == 1;
    }

    @Test
    void test4() {
        assert forestRepo.findDistinctById(1) != null;
    }
}
