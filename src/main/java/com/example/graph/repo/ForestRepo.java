package com.example.graph.repo;

import com.example.graph.entity.Forest;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.List;

public interface ForestRepo extends JpaRepository<Forest, Integer> {

//    @EntityGraph(attributePaths = {
//            "trees",
//            "trees.branches",
//            "trees.branches.leaves"
//    })
    @NonNull
    List<Forest> findAll();

    @EntityGraph("forest-graph")
    List<Forest> findAllById(int id);

    @EntityGraph(
            type = EntityGraph.EntityGraphType.FETCH,
            attributePaths = "trees"
    )
    Forest findDistinctById(int id);
}
