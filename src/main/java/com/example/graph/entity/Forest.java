package com.example.graph.entity;

import jakarta.persistence.*;
import lombok.Getter;

import java.util.Set;

@Getter
@Entity
@Table
@NamedEntityGraph(
        name = "forest-graph",
        attributeNodes = {
                @NamedAttributeNode(value = "trees", subgraph = "tree-graph")
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "tree-graph",
                        attributeNodes = {
                                @NamedAttributeNode(value = "branches")
                        }
                )
        }
)
public class Forest {

    @Id
    private int id;

    @OneToMany(mappedBy = "forest")//, fetch = FetchType.EAGER)
    private Set<Tree> trees;
}
