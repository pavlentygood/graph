package com.example.graph.entity;

import jakarta.persistence.*;
import lombok.Getter;

import java.util.Set;

@Getter
@Entity
@Table
public class Branch {

    @Id
    private int id;

    @ManyToOne
    private Tree tree;

    @OneToMany(mappedBy = "branch")//, fetch = FetchType.EAGER)
    private Set<Leaf> leaves;
}
