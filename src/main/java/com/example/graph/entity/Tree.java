package com.example.graph.entity;

import jakarta.persistence.*;
import lombok.Getter;

import java.util.Set;

@Getter
@Entity
@Table
public class Tree {

    @Id
    private int id;

    @ManyToOne
    private Forest forest;

    @OneToMany(mappedBy = "tree")//, fetch = FetchType.EAGER)
    private Set<Branch> branches;
}
