package com.example.graph.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;

@Getter
@Entity
@Table
public class Leaf {

    @Id
    private int id;

    @ManyToOne
    private Branch branch;
}
